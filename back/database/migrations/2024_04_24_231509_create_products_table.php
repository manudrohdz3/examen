<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid("vUuId")->primary();
            $table->string('vName');
            $table->string('vDescripcion');
            $table->integer('iHeight');
            $table->integer('iLength');
            $table->integer('iWidth');
            $table->timestamp("dAddDate")->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp("dUpdateDate")->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->charset = "utf8";
            $table->collation = "utf8_general_ci";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
