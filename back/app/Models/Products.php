<?php

namespace App\Models;

use App\Traits\UuIds;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory, UuIds;

    protected $table = "products";

    protected $primaryKey = "vUuId";

    const CREATED_AT = "dAddDate";
    const UPDATED_AT = "dUpdateDate";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "vName",
        "vDescripcion",
        "iHeight",
        "iLength",
        "iWidth",
        "dAddDate",
        "dUpdateDate",
    ];
}
