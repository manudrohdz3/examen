<?php

namespace App\Models;

use App\Traits\UuIds;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, UuIds;


    protected $table = "users";

    protected $primaryKey = "vUuId";

    public  $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "vName",
        "vLastName",
        "vPhone",
        "vImgProfile",
        "vEmail",
        "vPassword",
    ];
}
