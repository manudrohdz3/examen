<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            "data.attributes.name" => "required",
            "data.attributes.last_name" => "required",
            "data.attributes.phone" => "required|min:1|max:10",
            "data.attributes.email" => "required|email|unique:users,vEmail",
            "data.attributes.password" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json(["data" => [
                "errors" => $validator->errors()
            ]]);
        }
        $user = User::create([
            "vName" => $request->json("data.attributes.name"),
            "vLastName" => $request->json("data.attributes.last_name"),
            "vPhone" => $request->json("data.attributes.phone"),
            "vEmail" => $request->json("data.attributes.email"),
            "vPassword" => Hash::make($request->json("data.attributes.password")),
        ]);

        return response()->json([
            "data" => [
                "type" => "auth",
                "id" => $user->vUuId,
                "attributes" => [
                    "name" => $user->vName,
                    "last_name" => $user->vLastName,
                    "phone" => $user->vPhone,
                    "email" => $user->vEmail,
                ],
            ]
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            "data.attributes.user" => "required|email",
            "data.attributes.password" => "required",
        ]);
        if ($validator->fails()) {
            return response()->json(["data" => [
                "errors" => $validator->errors()
            ]]);
        }
        $user = User::where("vEmail", $request->json("data.attributes.user"))->first();
        if ($user) {
            if (Hash::check($request->json("data.attributes.password"), $user->vPassword)) {
                return response()->json([
                    "data" => [
                        "type" => 'auth',
                        "id" => $user->vUuId,
                        "attributes" => [
                            "name" => $user->vName,
                            "last_name" => $user->vLastName,
                            "phone" => $user->vPhone,
                            "email" => $user->vEmail,
                        ],
                        "meta" => [
                            "accessToken" => $user->createToken("API TOKEN")->plainTextToken,
                        ]
                    ]
                ]);
            } else {
                return response()->json(["data" => [
                    "errors" => [
                        "This user or password does not exist...!"
                    ]
                ]]);
            }
        } else {
            return response()->json(["data" => [
                "errors" => [
                    "This user or password does not exist...!"
                ]
            ]]);
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            'data.attributes.password' => "required|string",
            'data.attributes.identify' => "required|uuid"
        ]);
        if ($validator->fails()) {
            return response()->json(["data" => [
                "errors" => $validator->errors()
            ]]);
        }
        $user = User::where("vUuId", $request->json("data.attributes.identify"))->first();
        if ($user) {
            $user->vPassword = Hash::make($request->json("data.attributes.password"));
            $user->save();
            return response()->json([
                "data" => [
                    "type" => "auth",
                    "id" => $user->vUuId,
                    "attributes" => [
                        "name" => $user->vName,
                        "last_name" => $user->vLastName,
                        "phone" => $user->vPhone,
                        "email" => $user->vEmail,
                    ]
                ]
            ]);
        } else {
            return response()->json(["data" => [
                "errors" => [
                    "This user does not exist...!"
                ]
            ]]);
        }
    }

    public function logout(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            'data.attributes.identify' => "required|uuid"
        ]);
        if ($validator->fails()) {
            return response()->json(["data" => [
                "errors" => $validator->errors()
            ]]);
        }
        $user = User::where("vUuId", $request->json("data.attributes.identify"))->first();
        if (!$user) {
            return response()->json(["data" => [
                "errors" => [
                    "This user does not exist...!"
                ]
            ]]);
        }
        $user->tokens()->where('tokenable_id', $request->json("data.attributes.identify"))->delete();
        return response()->json([
            "data" => [
                "message" => [
                    "Token deleted"
                ]
            ]
        ]);
    }
}
