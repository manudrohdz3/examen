<?php

namespace App\JsonApi\Products;

use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;

class Validators extends AbstractValidators
{

    /**
     * The include paths a client is allowed to request.
     *
     * @var string[]|null
     *      the allowed paths, an empty array for none allowed, or null to allow all paths.
     */
    protected $allowedIncludePaths = [];

    /**
     * The sort field names a client is allowed send.
     *
     * @var string[]|null
     *      the allowed fields, an empty array for none allowed, or null to allow all fields.
     */
    protected $allowedSortParameters = ["name", "description"];

    /**
     * The filters a client is allowed send.
     *
     * @var string[]|null
     *      the allowed filters, an empty array for none allowed, or null to allow all.
     */
    protected $allowedFilteringParameters = ["name", "description"];

    /**
     * Get resource validation rules.
     *
     * @param mixed|null $record
     *      the record being updated, or null if creating a resource.
     * @param array $data
     *      the data being validated
     * @return array
     */
    protected function rules($record, array $data): array
    {
        if (is_null($record)) {
            return [
                "name" => "required|min:1",
                "description" => "required|string|min:1",
                "height" => "required|numeric|min:1",
                "length" => "required|numeric|min:1",
                "width" => "required|numeric|min:1",
            ];
        }
        return [
            "name" => "sometimes|min:1",
            "description" => "sometimes|numeric|min:1",
            "height" => "sometimes|numeric|min:1",
            "length" => "sometimes|numeric|min:1",
            "width" => "sometimes|numeric|min:1",
        ];
    }

    /**
     * Get query parameter validation rules.
     *
     * @return array
     */
    protected function queryRules(): array
    {
        return [
            //
        ];
    }
}
