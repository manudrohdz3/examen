<?php

namespace App\JsonApi\Products;

use CloudCreativity\LaravelJsonApi\Schema\SchemaProvider;

class Schema extends SchemaProvider
{

    /**
     * @var string
     */
    protected string $resourceType = 'products';

    /**
     * @param \App\Product $resource
     *      the domain record being serialized.
     * @return string
     */
    public function getId($resource): string
    {
        return (string) $resource->getRouteKey();
    }

    /**
     * @param \App\Product $resource
     *      the domain record being serialized.
     * @return array
     */
    public function getAttributes($resource): array
    {
        return [
            "name" => $resource->vName,
            "description" => $resource->vDescripcion,
            "height" => $resource->iHeight,
            "length" => $resource->iLength,
            "width" => $resource->iWidth,
            "creted_at" => $resource->dAddDate,
            "update_at" => $resource->dUpdateDate,
        ];
    }
}
