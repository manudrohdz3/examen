Para poder ejecutar el proyecto es necesario contar con docker instalado en la maquina, ya que cuenta con una serie de contenedores como son la base de datos (MYSQL,"mariadb"), cuenta con phpmyadmin como gestor de base de datos.

IMPORTANTE

1.-Instalar dependencias de composer con composer install en la terminal dentro de la carpeta de la carpeta del back
2.-Copiar y renombrar el archivo .env que se encuentra dentro de la carpeta back/.docker/laravel/.env.dev
3.-Donde esta el archivo docker-compose.yml ejecutar docker compose up -d
4.-Crear la base de datos proyecto desde phpmyadmin http://localhost:8080
5.-Conectarte al contenedor con el nombre "php-application" con el siguiente comando docker exec -it <id del contenedor| nombre del contendor> bash
6.-Bucar la carpeta /var/www/html  y ejecutar el comando php artisan migrate
7.-Con una coleccion postman que se encuentra en back/.docker/postman es posible probar los endpoints
